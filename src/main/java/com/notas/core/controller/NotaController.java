package com.notas.core.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.notas.core.entity.Nota;
import com.notas.core.model.Mnota;
import com.notas.core.service.NotaService;

@RestController 
@RequestMapping("/v1")
public class NotaController {

	@Autowired
	@Qualifier("service")
	private NotaService service;

	@GetMapping("/nota")
	public List<Mnota> getNotes(Pageable pageable) {
		return service.getByPagination(pageable);
	}

	@GetMapping("/nota/{title}")
	public List<Mnota> getByTitle(@PathVariable("title") String title) {
		return service.getByTitle(title);
	}

	@GetMapping("/nota/{name}/{title}")
	public List<Mnota> getByNameAndTitle(@PathVariable("name") String name,
			@PathVariable("title") String title) {
		return service.getByNameAndTitle(name, title);
	}

	@PostMapping("/nota")
	public boolean addNote(@RequestBody @Valid Nota nota) {
		return service.post(nota);
	}

	@PutMapping("/nota")
	public boolean updateNote(@RequestBody @Valid Nota nota) {
		return service.update(nota);
	}
	
	@DeleteMapping("/nota/{id}/{name}")
	public boolean deleteNote(@PathVariable("id") long id,
			@PathVariable("name") String name) {
		return service.delete(name, id);
	}
}
