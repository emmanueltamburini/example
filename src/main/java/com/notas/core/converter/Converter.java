package com.notas.core.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.notas.core.entity.Nota;
import com.notas.core.model.Mnota;

@Component("Converter")
public class Converter {
	public List<Mnota> convertList(List<Nota> notas) {
		List<Mnota> mnotas = new ArrayList<Mnota>();
		
		for(Nota nota: notas) {
			mnotas.add(new Mnota(nota));
		}
		
		return mnotas;
	}

}
