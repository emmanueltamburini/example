package com.notas.core.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name="Nota")
@Entity
public class Nota implements Serializable{
	private static final long serialVersionUID = 1L;

	public Nota() {
		super();
	}

	public Nota(long id, String name, String title, String content) {
		super();
		this.id = id;
		this.name = name;
		this.title = title;
		this.content = content;
	}

	@GeneratedValue
	@Id
	@Column(name="id")
	private long id;

	@Column(name="name")
	private String name;
	
	@Column(name="title")
	private String title;
	
	@Column(name="content")
	private String content;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
