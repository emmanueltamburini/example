package com.notas.core.model;

import com.notas.core.entity.Nota;

public class Mnota {
	public Mnota() {
		super();
	}

	public Mnota(Nota nota) {
		this(nota.getId(), nota.getName(),
				nota.getTitle(), nota.getContent());
	}

	public Mnota(long id, String name, String title, String content) {
		super();
		this.id = id;
		this.name = name;
		this.title = title;
		this.content = content;
	}

	private long id;
	private String name;
	private String title;
	private String content;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
}
