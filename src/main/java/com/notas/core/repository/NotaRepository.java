package com.notas.core.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.notas.core.entity.Nota;

@Repository("repository")
public interface NotaRepository 
	extends JpaRepository<Nota, Serializable>,
	PagingAndSortingRepository<Nota, Serializable>{
	Nota findByName(String name);

	List<Nota> findByTitle(String title);

	List<Nota> findByNameAndTitle(String name, String title);

	Nota findByNameAndId(String name, long id);
	
	Page<Nota> findAll(Pageable pageable);
}
