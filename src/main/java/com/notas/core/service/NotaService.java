package com.notas.core.service;

import java.util.List;

import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.notas.core.converter.Converter;
import com.notas.core.entity.Nota;
import com.notas.core.model.Mnota;
import com.notas.core.repository.NotaRepository;

@Service("service")
public class NotaService {

	@Autowired
	@Qualifier("repository")
	private NotaRepository repository;
	
	@Autowired
	@Qualifier("Converter")
	private Converter converter;
	
	private static final Log logger = LogFactory.getLog(NotaService.class);

	public List<Mnota> get() {
		logger.info("Getting notas");
		return converter.convertList(repository.findAll());
	}

	public boolean post(Nota nota) {
		logger.info("Building nota");
		try {
			repository.save(nota);
			logger.info("Nota has built");
			return true;
			}catch(Exception e) {
				logger.error("Nota has not built");
				return false;
			}
	}

	public boolean update(Nota nota) {
		logger.info("Updating nota");
		try {
			repository.save(nota);
			logger.info("Nota has updated");
			return true;
		}catch(Exception e) {
			logger.error("Nota has not updated");
			return false;
		}
	}

	public boolean delete(String name, long id) {
		logger.warn("Deleting nota");
		try {
			Nota nota = repository.findByNameAndId(name, id);
			repository.delete(nota);
			logger.info("Nota has deleted");
			return true;
		}catch(Exception e) {
			logger.error("Nota has not deleted");
			return false;
		}
	}
	
	public List<Mnota> getByNameAndTitle (String name, String title) {
		logger.info("Getting nota by name and title");
		return converter.convertList(repository.findByNameAndTitle(name, title));
	}
	
	public List<Mnota> getByTitle (String title) {
		logger.info("Getting nota by title");
		return converter.convertList(repository.findByTitle(title));
	}

	public List<Mnota> getByPagination  (Pageable pageable) {
		logger.info("Getting nota by pagination");
		return converter.convertList(repository.findAll(pageable).getContent());
	}

}
