package com.notas.core.converter;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.github.javafaker.Faker;
import com.notas.core.entity.Nota;

@SpringBootTest
public class ConverterTests {
	final static private Faker faker = new Faker();

	@Test
	public void convertListTest() {
		List<Nota> notas = new ArrayList<Nota>();

		for(int i = 0; i < faker.number().numberBetween(1, 10); i++) {
			notas.add(new Nota (faker.number().randomNumber(), faker.name().firstName(), faker.lorem().characters(0, 15), faker.lorem().characters(0, 50)));
		}
		
		Converter convert= new Converter();
		assertEquals(convert.convertList(notas) instanceof List<?> , true);
	}
	
}
