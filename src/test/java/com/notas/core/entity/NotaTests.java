package com.notas.core.entity;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import com.github.javafaker.Faker;

@SpringBootTest
public class NotaTests {
	final static private Faker faker = new Faker();

	@Test
	public void defaultConstructorTest() {
		Nota aux = new Nota ();
		assertEquals(aux instanceof Nota, true);
	}

	@Test
	public void parameterConstructorTest() {
		Nota aux = new Nota (faker.number().randomNumber(), faker.name().firstName(), faker.lorem().characters(0, 15), faker.lorem().characters(0, 50));
		assertEquals(aux instanceof Nota, true);
	}

	@Test
	public void getIdTest() {
		long id = faker.number().randomNumber();
		Nota aux = new Nota (id, faker.name().firstName(), faker.lorem().characters(0, 15), faker.lorem().characters(0, 50));
		assertEquals(id, aux.getId());
	}

	@Test
	public void getNameTest() {
		String name = faker.name().firstName();
		Nota aux = new Nota (faker.number().randomNumber(), name, faker.lorem().characters(0, 15), faker.lorem().characters(0, 50));
		assertEquals(name, aux.getName());
	}

	@Test
	public void getTitleTest() {
		String title = faker.lorem().characters(0, 15);
		Nota aux = new Nota (faker.number().randomNumber(), faker.name().firstName(), title, faker.lorem().characters(0, 50));
		assertEquals(title, aux.getTitle());
	}

	@Test
	public void getContentTest() {
		String content = faker.lorem().characters(0, 50);
		Nota aux = new Nota (faker.number().randomNumber(), faker.name().firstName(), faker.lorem().characters(0, 15), content);
		assertEquals(content, aux.getContent());
	}

	@Test
	public void setIdTest() {
		long id = faker.number().randomNumber();
		Nota aux = new Nota ();
		aux.setId(id);
		assertEquals(id, aux.getId());
	}

	@Test
	public void setNameTest() {
		String name = faker.name().firstName();
		Nota aux = new Nota ();
		aux.setName(name);
		assertEquals(name, aux.getName());
	}
	
	@Test
	public void setTitleTest() {
		String title = faker.lorem().characters(0, 15);
		Nota aux = new Nota ();
		aux.setTitle(title);
		assertEquals(title, aux.getTitle());
	}

	@Test
	public void setContentTest() {
		String content = faker.lorem().characters(0, 50);
		Nota aux = new Nota ();
		aux.setContent(content);
		assertEquals(content, aux.getContent());
	}
}
