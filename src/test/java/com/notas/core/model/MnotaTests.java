package com.notas.core.model;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import com.github.javafaker.Faker;
import com.notas.core.entity.Nota;

@SpringBootTest
public class MnotaTests {
	final static private Faker faker = new Faker();

	@Test
	public void defaultConstructorTest() {
		Mnota aux = new Mnota ();
		assertEquals(aux instanceof Mnota, true);
	}

	@Test
	public void parameterConstructorTest() {
		Mnota aux = new Mnota (faker.number().randomNumber(), faker.name().firstName(), faker.lorem().characters(0, 15), faker.lorem().characters(0, 50));
		assertEquals(aux instanceof Mnota, true);
	}

	@Test
	public void copyConstructorTest() {
		Nota copy = new Nota (faker.number().randomNumber(), faker.name().firstName(), faker.lorem().characters(0, 15), faker.lorem().characters(0, 50));
		Mnota aux = new Mnota (copy);
		assertEquals(aux instanceof Mnota, true);
	}
	
	@Test
	public void getIdTest() {
		long id = faker.number().randomNumber();
		Mnota aux = new Mnota (id, faker.name().firstName(), faker.lorem().characters(0, 15), faker.lorem().characters(0, 50));
		assertEquals(id, aux.getId());
	}

	@Test
	public void getNameTest() {
		String name = faker.name().firstName();
		Mnota aux = new Mnota (faker.number().randomNumber(), name, faker.lorem().characters(0, 15), faker.lorem().characters(0, 50));
		assertEquals(name, aux.getName());
	}

	@Test
	public void getTitleTest() {
		String title = faker.lorem().characters(0, 15);
		Mnota aux = new Mnota (faker.number().randomNumber(), faker.name().firstName(), title, faker.lorem().characters(0, 50));
		assertEquals(title, aux.getTitle());
	}

	@Test
	public void getContentTest() {
		String content = faker.lorem().characters(0, 50);
		Mnota aux = new Mnota (faker.number().randomNumber(), faker.name().firstName(), faker.lorem().characters(0, 15), content);
		assertEquals(content, aux.getContent());
	}

	@Test
	public void setIdTest() {
		long id = faker.number().randomNumber();
		Mnota aux = new Mnota ();
		aux.setId(id);
		assertEquals(id, aux.getId());
	}

	@Test
	public void setNameTest() {
		String name = faker.name().firstName();
		Mnota aux = new Mnota ();
		aux.setName(name);
		assertEquals(name, aux.getName());
	}
	
	@Test
	public void setTitleTest() {
		String title = faker.lorem().characters(0, 15);
		Mnota aux = new Mnota ();
		aux.setTitle(title);
		assertEquals(title, aux.getTitle());
	}

	@Test
	public void setContentTest() {
		String content = faker.lorem().characters(0, 50);
		Mnota aux = new Mnota ();
		aux.setContent(content);
		assertEquals(content, aux.getContent());
	}
}
